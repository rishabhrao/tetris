using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;
using UnityEngine.SceneManagement;

public class DatabaseManager : MonoBehaviour
{
    public static DatabaseManager instance;

    public string EmailValue = "";

    public InputField Email;
    public InputField Password;

    public DatabaseReference dbReference;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        dbReference = FirebaseDatabase.DefaultInstance.RootReference;
    }

    public void CreateUser()
    {
        User newUser = new User(Email.text, Password.text);
        string json = JsonUtility.ToJson(newUser);

        dbReference.Child("User").Child(Email.text).SetRawJsonValueAsync(json);

        EmailValue = Email.text;

        SceneManager.LoadScene("MainMenu");
    }

    public async void LoginUser()
    {
        DataSnapshot snapshot = await dbReference.Child("User").Child(Email.text).GetValueAsync();

        if (snapshot.Exists)
        {
            Debug.Log("Got DataSnapshot");

            string json = snapshot.GetRawJsonValue();
            User loggedInUser = JsonUtility.FromJson<User>(json);

            if (loggedInUser.password == Password.text)
            {
                Debug.Log("YES!");

                EmailValue = Email.text;

                SceneManager.LoadScene("MainMenu");
            }
            else
            {
                Debug.Log("NO!");

                Password.text = "";
            }
        }
        else
        {
            Debug.Log("Unable to get DataSnapshot");

            Password.text = "";
        }
    }

    public async void setNewScoreAsHighScoreIfNeeded(int newScore)
    {
        Debug.Log("Checking if this value is more than the one stored in DB");
        Debug.Log(newScore);

        DataSnapshot snapshot = await dbReference.Child("User").Child(EmailValue).GetValueAsync();

        if (snapshot.Exists)
        {
            Debug.Log("Got DataSnapshot");

            string json = snapshot.GetRawJsonValue();
            User loggedInUser = JsonUtility.FromJson<User>(json);

            int highScore = loggedInUser.highScore;

            if (newScore > highScore) {
                // set newScore as the highScore

                User newUser = new User(loggedInUser.email, loggedInUser.password, newScore);
                string newJson = JsonUtility.ToJson(newUser);

                dbReference.Child("User").Child(Email.text).SetRawJsonValueAsync(newJson);
            }
        }
        else
        {
            Debug.Log("Unable to get DataSnapshot");
        }

    }
}
