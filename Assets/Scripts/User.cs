using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User
{
    public string email;
    public string password;
    public int highScore;

    public User(string email, string password, int highScore = 0)
    {
        this.email = email;
        this.password = password;
        this.highScore = highScore;
    }
}
