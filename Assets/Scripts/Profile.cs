using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Firebase.Database;

public class Profile : MonoBehaviour
{
    Text email;
    Text highScore;

    // Start is called before the first frame update
    void Start()
    {
        email = GameObject.Find("Email").GetComponent<Text> ();
        highScore = GameObject.Find("Highscore").GetComponent<Text> ();

        email.text = "Email: " + DatabaseManager.instance.EmailValue;

        GetHighScore();
    }

    public async void GetHighScore()
    {
        DataSnapshot snapshot = await DatabaseManager.instance.dbReference.Child("User").Child(DatabaseManager.instance.EmailValue).GetValueAsync();

        if (snapshot.Exists)
        {
            Debug.Log("Got DataSnapshot");

            string json = snapshot.GetRawJsonValue();
            User loggedInUser = JsonUtility.FromJson<User>(json);

            highScore.text = "Highscore: " + loggedInUser.highScore;
        }
        else
        {
            Debug.Log("Unable to get DataSnapshot");
        }
    }

    public void BackButton()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
